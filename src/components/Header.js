import React from "react"

export const Header = ({data, style}) => {
  const {title, description} = data;
  return (
    <header style={style.header}>
        <div className="bg-dark collapse" id="navbarHeader">
            <div className="container">
            <div className="row">
                <div className="col-sm-8 col-md-7 py-4">
                <h4>Informations</h4>
                { description.map(d => <div className="text-muted">{d}</div>) }
                </div>
                {/* <div className="col-sm-4 offset-md-1 py-4">
                <h4 className="text-white">Contact</h4>
                 <ul className="list-unstyled">
                    <li><a href="#" className="text-white">Follow on Twitter</a></li>
                    <li><a href="#" className="text-white">Like on Facebook</a></li>
                    <li><a href="#" className="text-white">Email me</a></li>
                </ul> 
                </div> */}
            </div>
            </div>
        </div>
        <div className="navbar navbar-dark bg-dark shadow-sm">
            <div className="container">
            <a href="/" className="navbar-brand d-flex align-items-center">
                {/* <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" aria-hidden="true" className="me-2" viewBox="0 0 24 24"><path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path><circle cx="12" cy="13" r="4"></circle></svg> */}
                <strong style={style.header}>{title}</strong>
            </a>
            <button className="navbar-toggler collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            </div>
        </div>
    </header>
  )
}