import React from "react";
import { SubProduct } from "./SubProduct";

export const Product = ({data, style}) => {
    const { title, price, description, subproducts} = data;
    const hasSubProducts = subproducts && subproducts.length;
    return (
        <div className="col">
          <div className="card shadow-sm" style={style.product}>
            <div className="card-body">
              <div className="d-flex justify-content-between align-items-center">
                <div className={`card-text product-title ${ hasSubProducts ? "mb-2": ""}`}>{title}</div>
                <strong>{price}€</strong>
              </div>
              { subproducts && (
                <div className="subproducts">
                  {subproducts.filter(s => !s.disabled).map((s, index) => <SubProduct key={index} data={s} style={style} /> ) }
                </div>
              )}
              { description && <div className="card-text description product-description mt-2">{description}</div> }
            </div>
          </div>
        </div>
    )
}