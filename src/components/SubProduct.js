import React from "react";

export const SubProduct = ({data, style}) => {
    const { title, description} = data;

    return <span className="badge subproduct" style={style.subproduct}>{title} {description && <small>({description})</small>}</span>
}