import React from "react";
import { Product } from "./Product"

export const Category = ({data, style}) => {
    const { title, products, description } = data;
    return (
        <div>
            <h2 className="mb-3 mt-5 category" style={style.category}>{title} <small>{description}</small></h2>
            <div className="container mb-3">
                <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                    { products.filter(s => !s.disabled).map((p, index) => <Product key={index} data={p} style={style} /> )}
                </div>
            </div>
        </div>
    )
}