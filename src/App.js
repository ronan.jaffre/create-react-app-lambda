import React from "react";
import "./App.css";
import { Header } from "./components/Header";
import { DATA } from "./data/data";
import { Details } from "./components/Details";
import { Category } from "./components/Category";

const App = () => {
  document.getElementById("html").style = `background-color: ${DATA.style.details.backgroundColor}`;

  return (
    <div className="App" style={{backgroundColor: DATA.style.details.backgroundColor}}>
      <Header data={DATA} style={DATA.style} />
      <Details details={DATA.details} style={DATA.style} />
      { DATA.categories.map((c, index) => <Category key={index} data={c} style={DATA.style} /> )}
    </div>
  )
}

export default App
