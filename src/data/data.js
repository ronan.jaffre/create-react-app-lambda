export const DATA = {
  title: "Hlounge",
  description: [
    "Lounge restaurant bordelais",
    "Adresse: ",
    "Téléphone: 06 67 01 96 87",
  ],
  style: {
    header: {
      color: "gold",
    },
    details: {
      color: "white",
      backgroundColor: "#202020",
    },
    category: {
      color: "gold",
      backgroundColor: "#202020",
    },
    product: {
      color: "white",
      backgroundColor: "rgb(36, 37, 38)",
    },
    subproduct: {
      color: "white",
      backgroundColor: "#404040",
    },
  },
  details: {
    title: "Carte",
  },
  categories: [
    {
      title: "Nos salades",
      products: [
        {
          title: "Base laitue",
          price: "9",
          subproducts: [
            {
              title: "Crouton",
            },
            {
              title: "Saumon",
            },
            {
              title: "Maïs",
            },
            {
              title: "Thon",
            },
            {
              title: "Comcombre",
            },
            {
              title: "Tomate cerise",
            },
            {
              title: "Avocat",
            },
            {
              title: "Oignons frits",
            },
          ],
        },
        {
          title: "Base pâte",
          price: "11",
          subproducts: [
            {
              title: "Poulet",
            },
            {
              title: "Maïs",
            },
            {
              title: "Thon",
            },
            {
              title: "Oignon fries",
            },
            {
              title: "Tomate cerise",
            },
            {
              title: "Crouton",
            },
          ],
        },
      ],
    },
    {
      title: "Nos paninis",
      products: [
        {
          title: "Saumon",
          price: "7",
        },
        {
          title: "Poulet curry",
          price: "8",
        },
        {
          title: "4 fromages",
          price: "6",
        },
      ],
    },
    {
      title: "Nos burgers",
      products: [
        {
          title: "Crispy meat",
          price: "7",
          subproducts: [
            { title: "Steak haché maison" },
            { title: "Salade" }, 
            { title: "Tomates" },
            { title: "Sauce maison poivre"},
            { title: "Oignons frits"},
            { title: "Cheddar" },
          ]
        },
        {
          title: "Bacon",
          price: "7",
          subproducts: [
            { title: "Steak haché maison" },
            { title: "Salade" },
            { title: "Tomates" },
            { title: "Concombre" },
            { title: "Sauce béarnaise" },
            { title: "Cheddar" },
          ]
        },
      ],
    },
    {
      title: "Nos assiettes",
      products: [
        {
          title: "Kefta frites salade boisson",
          price: "15",
        },
        {
          title: "Merguez frites salade boisson",
          price: "15",
        },
        {
          title: "Mix frites salade boisson",
          price: "17",
        },
      ],
    },
    {
      title: "Nos cocktails",
      products: [
        {
          title: "Mojito",
          price: "7",
          subproducts: [
            { title: "Fraise" },
            { title: "Pêche" },
            { title: "Menthe" },
          ],
        },
        {
          title: "Bora Bora",
          description: "A base de mangue",
          price: "7",
        },
        {
          title: "Pina colada",
          description: "Base noix de coco",
          price: "7",
        },
        {
          title: "Blue artic",
          description: "Base de framboise myrtille",
          price: "7",
        },
      ],
    },
    {
      title: "Nos boissons",
      products: [
        {
          title: "Coca",
          price: "2",
        },
        {
          title: "Coca cherry",
          price: "2",
        },
        {
          title: "Oasis tropical",
          price: "2",
        },
        {
          title: "Oasis fraise",
          price: "2",
        },
        {
          title: "Oasis pomme poire",
          price: "2",
        },
        {
          title: "Oasis pomme casis",
          price: "2",
        },
        {
          title: "Fanta",
          price: "2",
        },
        {
          title: "Seven up tropical",
          price: "2",
        },
        {
          title: "Sirop",
          price: "2",
        },
      ],
    },
    {
      title: "Nos chichas",
      products: [
        {
          title: "Ice plosion",
          price: "20",
          description: "Tête supplémentaire: 10€",
          subproducts: [
            {
              title: "Bonbon frais",
            },
          ],
        },
        {
          title: "Roboto",
          price: "20",
          description: "Tête supplémentaire: 10€",
          subproducts: [
            {
              title: "Ananas",
            },
            {
              title: "Passion",
            },
          ],
        },
        {
          title: "Policeman",
          price: "20",
          description: "Tête supplémentaire: 10€",
          subproducts: [
            {
              title: "Pasteque",
            },
            {
              title: "Litchi",
            },
          ],
        },
        {
          title: "Love",
          price: "20",
          description: "Tête supplémentaire: 10€",
          subproducts: [
            {
              title: "Passion",
            },
            {
              title: "Pastèque",
            },
            {
              title: "Menthe",
            },
            {
              title: "Melon",
            },
          ],
        },
        {
          title: "Mi amor",
          price: "20",
          description: "Tête supplémentaire: 10€",
          subproducts: [
            {
              title: "Ananas",
            },
            {
              title: "Banane",
            },
          ],
        },
        {
          title: "Bawai",
          price: "20",
          description: "Tête supplémentaire: 10€",
          subproducts: [
            {
              title: "Mangue",
            },
            {
              title: "Ananas",
            },
            {
              title: "Pêche",
            },
          ],
        },
      ],
    },    
    {
      title: "Nos tapas",
      products: [
        {
          title: "Fries",
          price: "16",
          subproducts: [
            {
              title: "Nems",
            },
            {
              title: "Crevette tempura",
            },
            {
              title: "Calamars rings",
            },
            {
              title: "Nugets",
            },
            {
              title: "Pince de crabe",
            },
            {
              title: "Stick mozza",
            },
          ],
        },
        {
          title: "Charcuterie",
          price: "16",
          subproducts: [
            {
              title: "Bœuf séché",
            },
            {
              title: "Saucisson sec",
            },
            {
              title: "Mortadelle",
            },
            {
              title: "Fromage",
            },
            {
              title: "Cornichons",
            },
            {
              title: "Beurre",
            },
          ],
        },
        {
          title: "Royal",
          price: "40",
          description: "Mélange des deux pour plus de 4 personnes",
        },
      ],
    },
    
    {
      title: "Nos sandwichs",
      descriptions: "(10e formule frites boisson)",
      products: [
        {
          title: "Kefta maison",
          price: "10",
        },
        {
          title: "Merguez",
          price: "10",
          subproducts: [
            {
              title: "Mayonnaise",
            },
            {
              title: "Ketchup",
            },
            {
              title: "Algérienne",
            },
            {
              title: "Barbecue",
            },
            {
              title: "Biggy",
            },
          ],
        },
      ],
    },
    {
      title: "Pâtes",
      products: [
        {
          title: "Penne",
          price: "13",
          subproducts: [
            {
              title: "Saumon crème fraiche",
            },
            {
              title: "Forestière (poulet, crême fraiche, champignon)"
            },
            {
              title: "Pesto (poulet, pesto)"
            }
          ],
        },
        {
          title: "Tagliatelle",
          price: "13",
          subproducts: [
            
            {
              title: "Saumon crème fraiche",
            },
            {
              title: "Forestière (poulet, crême fraiche, champignon)"
            },
            {
              title: "Pesto (poulet, pesto)"
            }
          ],
        },
      ],
    },
    {
      title: "Nos smoothies",
      products: [
        {
          title: "Banane",
          price: "7",
        },
        {
          title: "Fraise",
          price: "7",
        },
        {
          title: "Avocat",
          price: "7",
        },
      ],
    },
    {
      title: "Nos milkshakes",
      products: [
        {
          title: "Kinder",
          price: "7",
        },
        {
          title: "Oreo",
          price: "7",
        },
        {
          title: "Speculos",
          price: "7",
        },
        {
          title: "Daim",
          price: "7",
        },
        {
          title: "Vanille",
          price: "7",
        },
        {
          title: "Fraise",
          price: "7",
        },
      ],
    },
    {
      title: "Nos desserts",
      products: [
        {
          title: "Pancake",
          price: "8",
          subproducts: [
            {
              title: "Nutella",
            },
            {
              title: "Kinder",
            },
            {
              title: "speculos",
            },
            {
              title: "Oreo",
            },
            {
              title: "Fraise lion",
            },
            {
              title: "Daim",
            },
          ],
        },
        {
          title: "Gauffre",
          price: "7",
          subproducts: [
            {
              title: "Nutella",
            },
            {
              title: "Chantilly",
            },
            {
              title: "Kinder",
            },
            {
              title: "Oreo",
            },
            {
              title: "Fraise lion",
            },
            {
              title: "Daim",
            },
          ],
        },
        {
          title: "Tiramisu maison",
          price: "3,50",
        },
        {
          title: "Pot de bonbons",
          price: "2,50",
        },
      ],
    },
  ],
};
